<%-- 
    Document   : mainPage
    Created on : Jun 28, 2016, 3:12:26 PM
    Author     : Clifford
--%>

    <%@page contentType="text/html" pageEncoding="UTF-8"%>
        <!DOCTYPE html>
        <html>

        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Wealth Management System</title>
            <link rel="stylesheet" href="css/normalize.css">
            <link rel="stylesheet" href="css/case.css">
            <link href="https://fonts.googleapis.com/css?family=Assistant:300,400" rel="stylesheet">
        </head>

        <body>
            <div class="outer-navbar">

                <div class="inner-navbar">
                    <a href="mainPage.jsp"><img class="menubutton menu-logo" src="assets/ubs_regular_rgb_26.svg"></a>
                    <a class="menubutton" href="adviceMethods.jsp">Choose a method of advice</a>
                    <a class="menubutton" href="adviceTiming.jsp">Choose when to receive advice</a>
                    <a class="menubutton" href="checkAssets.jsp">Check your Assets</a>
                    <a class="menubutton" href="countries.jsp">Contact Us</a>
                    <a class="menubutton" href="products.jsp">Our Products</a>
                </div>
            </div>
            
            <div class="gradientbar"></div>
            <img class="banner-img" src="assets/pexels-photo-29594.jpg" alt=""/>
            <div class="banner-text">
                <h1>Welcome to Elite Bank</h1>
                <h3>Services Available. Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                    Ut velit dolor, pretium nec ante eu, porttitor egestas sem. Maecenas in tempor nisl. 
                    Nulla facilisi.</h3>
            </div>  
     
            <div class="container">
            </div>
            
        </body>
        </html>