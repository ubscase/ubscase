<%-- 
    Document   : eliteproduct2
    Created on : Jun 29, 2016, 2:07:48 PM
    Author     : Clifford
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Elite Product 2</title>
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/case.css">
        <link href="https://fonts.googleapis.com/css?family=Assistant:300,400" rel="stylesheet">
    </head>
    <body>
                <div class="outer-navbar">

                <div class="inner-navbar">
                     <a href="mainPage.jsp"><img class="menubutton menu-logo" src="assets/ubs_regular_rgb_26.svg"></a>
                    <a class="menubutton" href="adviceMethods.jsp">Choose a method of advice</a>
                    <a class="menubutton" href="adviceTiming.jsp">Choose when to receive advice</a>
                    <a class="menubutton" href="checkAssets.jsp">Check your Assets</a>
                    <a class="menubutton" href="countries.jsp">Contact Us</a>
                    <a class="menubutton" href="products.jsp">Our Products</a>
                </div>
            </div>
            
            <div class="gradientbar"></div>
    <div class="container center">
        <br>
        <h1>Information about Elite Product 2</h1>
        <br>
        <img class="image center" src="assets/elite2.png" alt=""/>
        <br>
        <h3>enean dapibus quam augue, nec tempor diam ullamcorper eget. Fusce scelerisque finibus tortor at aliquet. 
            Vestibulum eu nunc eleifend, placerat ex at, sollicitudin nunc. Duis aliquam sollicitudin tristique. 
            Donec enim risus, sagittis nec nisl nec, efficitur finibus nibh. </h3>
        <br>
        <a href ="transaction.jsp"><input type="button" value="Click to Buy" name="buyProduct" /></a>
        <a href ="compare.jsp?product=EliteProduct1"><input type ="button" value ="Compare Products" name ="compareProduct"/></a>

    </div>    
            
    <div class="footer">
        <div class="inner-footer">©Copyright 2016. All Rights Reserved.
        </div>
    </div>
    
       </body>
</html>
