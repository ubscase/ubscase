<%-- 
    Document   : adviceMethods
    Created on : Jun 28, 2016, 5:00:06 PM
    Author     : Clifford
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Advice Timing</title>
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/case.css">
        <link href="https://fonts.googleapis.com/css?family=Assistant:300,400" rel="stylesheet">
    </head>
                <div class="outer-navbar">

                <div class="inner-navbar">
                     <a href="mainPage.jsp"><img class="menubutton menu-logo" src="assets/ubs_regular_rgb_26.svg"></a>
                    <a class="menubutton" href="adviceMethods.jsp">Choose a method of advice</a>
                    <a class="menubutton" href="adviceTiming.jsp">Choose when to receive advice</a>
                    <a class="menubutton" href="checkAssets.jsp">Check your Assets</a>
                    <a class="menubutton" href="countries.jsp">Contact Us</a>
                    <a class="menubutton" href="products.jsp">Our Products</a>
                </div>
            </div>
            
            <div class="gradientbar"></div>
            <img class="banner-img" src="assets/advice.jpg" alt=""/>
            <div class="banner-text">
                <h1>Advicements</h1>

                <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                    Nullam sit amet nunc sapien. Pellentesque et ex sed lectus eleifend finibus.</h3>
            </div>  
    
    <div class="container">
        <h1>This is the page which shows methods of Advice</h1>
        <h3>We understand that everyone has their own preference. That is why the choice is yours when it comes to advice methods. <br>Phone, Email, Snail Mail. We got you. </h3>
            </div>
        <h1></h1>
        
        <div class="footer">
        <div class="inner-footer">©Copyright 2016. All Rights Reserved.
        </div>
    </div>
    </body>
        
</html>
