/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package defaultPackage;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author Clifford
 */
public class productDAO {

    ArrayList<Product> productList = new ArrayList<Product>();

    public void addProduct(Product p) {
        productList.add(p);
    }

    public ArrayList<Product> retrieveAll(String list) {
        populateList(list);

        return productList;
    }

    public Product retrieve(String list, String productName) {
        populateList(list);
        System.out.println(productList.size());
        for (Product p : productList) {

            String name = p.getName();
            if (productName.equals(name)) {
                System.out.println(p);
                return p;
            }
        }
        return null;
    }

    public void populateList(String list) {
        String csvFile = "C:/Users/Clifford/Desktop/ubs/UBS Case/data/" + list;
        BufferedReader br = null;
        String line = "";
        //ArrayList<String> productInfo = new ArrayList<String>();

        try {
            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
                String[] productInfo = line.split(",");
                String productName = productInfo[0];
                String sProductPrice = productInfo[1];
                double productPrice = Double.parseDouble(sProductPrice);
                String productDescription = productInfo[2];

                Product toAdd = new Product(productName, productPrice, productDescription);
                productList.add(toAdd);

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public int comparePrice(Product eliteBankProd, Product otherBankProd) {
        int price = 0;
        double priceDifference = eliteBankProd.getPrice() - otherBankProd.getPrice();
        if (priceDifference > 0) {
            price = 1;
        } else if (priceDifference < 0) {
            price = -1;
        } else {
            price = 0;
        }

        return 0;

    }

}
