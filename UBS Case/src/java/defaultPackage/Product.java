/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package defaultPackage;

/**
 *
 * @author Clifford
 */
public class Product {
    
    private String productName="";
    private double productPrice=0;
    private String productDescription="";
    
    public Product(String productName, double productPrice, String productDescription){
        this.productName = productName;
        this.productPrice = productPrice;
        this.productDescription = productDescription;
        
    }
    public String getName(){
        return productName;
    }
    
    public double getPrice(){
        return productPrice;
    }
    
    public String getProductDescription(){
        return productDescription;
    }
    
    public void setName(String productName){
        this.productName = productName;
    }
    
    public void setPrice(double productPrice){
        this.productPrice = productPrice;
    }
    
    public void setDescription(String productDescription){
        this.productDescription = productDescription;
    }
}
