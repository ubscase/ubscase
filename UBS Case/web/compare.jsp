<%-- 
    Document   : compare
    Created on : Jul 4, 2016, 1:44:13 PM
    Author     : Clifford
--%>


<%@page import="java.util.ArrayList"%>
<%@page import="defaultPackage.Product"%>
<%@page import="defaultPackage.productDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Compare Products</title>
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/case.css">
        <link href="https://fonts.googleapis.com/css?family=Assistant:300,400" rel="stylesheet">
        <script language="javascript">
            function SelectShow() {
                <% 
                    productDAO pDAO = new productDAO();
                    
                    %>
               
                switch (document.getElementById("s1").value) {
                    case "cp1":
                                    

                        <%
                        String productName = "Competitor Product 1";
                        %>
           
                        <%
                        Product p = pDAO.retrieve("competitorProducts.csv" , productName);
                        System.out.println(p);
                        %>
                        
                        
                        //document.getElementById("s1").value;
                        
                        document.getElementById("demo").innerHTML = "String is" + <%= p %>;
                      
                        //document.getElementById("s1").submit();
                        //window.location("");
                        
                        break;

                    case "cp2":
                        window.location = "eliteproduct2.jsp";
                        break;

                    case "cp3":
                        window.location = "eliteproduct3.jsp";
                        break;

                    default:
                        window.location = "mainPage.jsp";
                        break;
                }
            }

        </script>
    </head>
    <body>
                <div class="outer-navbar">

                <div class="inner-navbar">
                     <a href="mainPage.jsp"><img class="menubutton menu-logo" src="assets/ubs_regular_rgb_26.svg"></a>
                    <a class="menubutton" href="adviceMethods.jsp">Choose a method of advice</a>
                    <a class="menubutton" href="adviceTiming.jsp">Choose when to receive advice</a>
                    <a class="menubutton" href="checkAssets.jsp">Check your Assets</a>
                    <a class="menubutton" href="countries.jsp">Contact Us</a>
                    <a class="menubutton" href="products.jsp">Our Products</a>
                </div>
            </div>
            
            <div class="gradientbar"></div>
        <div class="container center">
        <h1>Comparison page</h1>
        <% 
        int counter = 1;
        String value = "";
       // productDAO pDAO = new productDAO();
        pDAO = new productDAO();
        ArrayList<Product> competitorList = pDAO.retrieveAll("competitorProducts.csv");        
        %>
        
        <SELECT id="s1" NAME="section" onChange="SelectShow()">
        <option value =""></option>

            <% for (Product c : competitorList) {

                    value = "cp" + counter;

            %>



            <option value= <%= value%>><%= c.getName()%></option>


            <%
                    counter++;

                }%>

        </SELECT>
        </div>
                <p id="demo"></p>
        <div class="footer">
        <div class="inner-footer">©Copyright 2016. All Rights Reserved.
        </div>
    </div>
    </body>
</html>
