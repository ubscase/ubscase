<%-- 
    Document   : products
    Created on : Jun 28, 2016, 4:59:12 PM
    Author     : Clifford
--%>

<%@page import="defaultPackage.Product"%>
<%@page import="java.util.ArrayList"%>
<%@page import="defaultPackage.productDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Products</title>
            <link rel="stylesheet" href="css/normalize.css">
            <link rel="stylesheet" href="css/case.css">
            <link href="https://fonts.googleapis.com/css?family=Assistant:300,400" rel="stylesheet">
        <title>Our Products</title>
        <script language="javascript">
            function SelectRedirect() {
                switch (document.getElementById('s1').value) {
                    case "ep1":
                        window.location = "eliteproduct1.jsp";
                        break;

                    case "ep2":
                        window.location = "eliteproduct2.jsp";
                        break;

                    case "ep3":
                        window.location = "eliteproduct3.jsp";
                        break;

                    default:
                        window.location = "mainPage.jsp";
                        break;
                }
            }

        </script>
    </head>
    <body>
        <div class="outer-navbar">

                <div class="inner-navbar">
                    <a href="mainPage.jsp"><img class="menubutton menu-logo" src="assets/ubs_regular_rgb_26.svg"></a>
                    <a class="menubutton" href="adviceMethods.jsp">Choose a method of advice</a>
                    <a class="menubutton" href="adviceTiming.jsp">Choose when to receive advice</a>
                    <a class="menubutton" href="checkAssets.jsp">Check your Assets</a>
                    <a class="menubutton" href="countries.jsp">Contact Us</a>
                    <a class="menubutton" href="products.jsp">Our Products</a>
                </div>
            </div>
            
            <div class="gradientbar"></div>
            <img class="banner-img" src="assets/product.jpg" alt=""/>
            <div class="banner-text">
                <h1>Products</h1>
                <h3>
                    Cras sollicitudin quis diam non scelerisque.
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                    Duis lacus nunc, vestibulum quis vulputate sit amet, vehicula sit amet nunc.</h3>
            </div>  
            <div class="container">
            <h1>Please choose any of the following products to learn more about it.</h1>
            
       
        <%
            int counter = 1;
            String value = "";
            productDAO pDAO = new productDAO();
            ArrayList<Product> productList = pDAO.retrieveAll("products.csv");%>

        <SELECT id="s1" NAME="section" onChange="SelectRedirect();">
            <option value =""></option>

            <% for (Product p : productList) {

                    value = "ep" + counter;

            %>



            <option value= <%= value%>><%= p.getName()%></option>


            <%
                    counter++;

                }%>

        </SELECT>




</div>
                <div class="footer">
        <div class="inner-footer">©Copyright 2016. All Rights Reserved.
        </div>
    </div>
    </body>  
        
</html>
