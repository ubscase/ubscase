<%-- 
    Document   : checkAssets
    Created on : Jun 28, 2016, 4:59:34 PM
    Author     : Clifford
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Check Assets</title>
            <link rel="stylesheet" href="css/normalize.css">
            <link rel="stylesheet" href="css/case.css">
            <link href="https://fonts.googleapis.com/css?family=Assistant:300,400" rel="stylesheet">
        </head>

        <body>
            <div class="outer-navbar">

                <div class="inner-navbar">
                    <a href="mainPage.jsp"><img class="menubutton menu-logo" src="assets/ubs_regular_rgb_26.svg"></a>
                    <a class="menubutton" href="adviceMethods.jsp">Choose a method of advice</a>
                    <a class="menubutton" href="adviceTiming.jsp">Choose when to receive advice</a>
                    <a class="menubutton" href="checkAssets.jsp">Check your Assets</a>
                    <a class="menubutton" href="countries.jsp">Contact Us</a>
                    <a class="menubutton" href="products.jsp">Our Products</a>
                </div>
            </div>
            
            <div class="gradientbar"></div>
            <img class="banner-img" src="assets/assets.jpg" alt=""/>
            <div class="banner-text">
                <h1>Check your assets</h1>
                <h3>Duis lacus nunc, vestibulum quis vulputate sit amet, vehicula sit amet nunc.
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras sollicitudin quis diam non scelerisque.</h3>
            </div>  
     
            <div class="container">
            </div>
           <div class="footer">
        <div class="inner-footer">©Copyright 2016. All Rights Reserved.
        </div>
    </div> 
        </body>
            
        </html>
