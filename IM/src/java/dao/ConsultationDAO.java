/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import entity.Consultation;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

/**
 *
 * @author jeremy.seow.2014
 */
public class ConsultationDAO {

    private String filePath;
    private ArrayList<Consultation> consultations;
    private String[] header;

    public ConsultationDAO(String filePath) {
        this.filePath = filePath;
    }

    public ArrayList<Consultation> getConsultations() throws Exception {
        if (consultations == null) {
            readConsultations();
        }
        return consultations;
    }

    public void readConsultations() throws Exception {
        CSVReader reader = new CSVReader(new FileReader(filePath), CSVWriter.DEFAULT_SEPARATOR, '\n');
        String[] nextLine;
        consultations = new ArrayList<Consultation>();

        header = reader.readNext();
        System.out.println(header[1]);
        while ((nextLine = reader.readNext()) != null) {
            String customer = nextLine[0];
            String timeslot = nextLine[1];
            String advisor = nextLine[2];

            Consultation consultation = new Consultation(customer, timeslot, advisor);
            consultations.add(consultation);
        }
        reader.close();
    }

    public void saveConsultations() throws Exception {
        CSVWriter writer = new CSVWriter(new FileWriter(filePath), CSVWriter.DEFAULT_SEPARATOR, CSVWriter.NO_QUOTE_CHARACTER);
        String[] entry = new String[3];
        writer.writeNext(header);

        for (Consultation c : consultations) {
            entry[0] = c.getCustomer();
            entry[1] = c.getTimeSlot();
            entry[2] = c.getAdvisor();

            writer.writeNext(entry);
        }
        writer.close();
    }
}
