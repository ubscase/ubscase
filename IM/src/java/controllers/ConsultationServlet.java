/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import dao.ConsultationDAO;
import entity.Consultation;
import entity.Customer;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author jeremy.seow.2014
 */
@WebServlet(name = "ConsultationServlet", urlPatterns = {"/ConsultationServlet"})
public class ConsultationServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm");
            String date = request.getParameter("day");
            String time = request.getParameter("time");
            String comment = request.getParameter("comment");
            
            HttpSession session = request.getSession();
            Customer customer = (Customer)session.getAttribute("loggedIn");

            if (date == null || date.isEmpty() || time == null || time.isEmpty()
                    || comment == null) {
                System.out.println("empty");
            } else {
                String booking = date + " " + time;
                System.out.println(booking);

                try {
                    ConsultationDAO cDAO = new ConsultationDAO("D:\\UBSCase\\IM\\files\\ConsultationSlot.csv");
                    ArrayList<Consultation> consultations = cDAO.getConsultations();
                    boolean occupied = false;
                    
                    for(Consultation c: consultations){
                        
                        if(c.getTimeSlot().equals(booking)){
                            occupied = true;
                            break;
                        }
                    }
                    System.out.println("1");
                    if(!occupied){
                        System.out.println("2");
                        consultations.add(new Consultation("Jeremy", booking, "Fag"));
                        cDAO.saveConsultations();
                    }
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }

            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
