/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author jeremy.seow.2014
 */
public class Consultation {
    private String customer;
    private String timeSlot;
    private String advisor;

    public Consultation(String customer, String timeSlot, String advisor) {
        this.customer = customer;
        this.timeSlot = timeSlot;
        this.advisor = advisor;
    }
    
    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getTimeSlot() {
        return timeSlot;
    }

    public void setTimeSlot(String timeSlot) {
        this.timeSlot = timeSlot;
    }

    public String getAdvisor() {
        return advisor;
    }

    public void setAdvisor(String advisor) {
        this.advisor = advisor;
    }

}
