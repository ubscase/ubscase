/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author jeremy.seow.2014
 */
public class Customer {
    private String name;
    private String assignedAdvisor;

    public Customer(String name, String assignedAdvisor) {
        this.name = name;
        this.assignedAdvisor = assignedAdvisor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAssignedAdvisor() {
        return assignedAdvisor;
    }

    public void setAssignedAdvisor(String assignedAdvisor) {
        this.assignedAdvisor = assignedAdvisor;
    }
    
}
